const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express({strict:true});

const {router} = require('./routes/route');


app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());
app.use((req, res, next)=>{
    res.setHeader("Acces-Control-Allow-Origin",'*');
    res.setHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE");
    res.setHeader("Access-Control-Allow-Headers","Content-Type");
    next();
});


app.get('/', (req, res)=>{
    res.send("Welcome to Atlas lab");
});

app.use('/api/', router);

mongoose.connect(`mongodb://localhost:27017/`,{
    useNewUrlParser:true,
    useCreateIndex:true,
    bufferCommands:false,
    useFindAndModify:false,
    dbName:'atlasLab',
    useUnifiedTopology:true
}).then(result=>{
    app.listen(3000);
}).catch(err=>{
    console.log(err);
    return;
})