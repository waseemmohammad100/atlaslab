const {body, validationResult,} = require('express-validator');

exports.addUserValidator = [
    
    body('email')
    .trim()
    .not().isEmpty()
    .withMessage('Email cannot be empty'),

    body('name')
    .trim()
    .not().isEmpty()
    .withMessage('Name cannot be empty'),

    body('contactNo')
    .trim()
]

exports.deleteUserValidator = [
    body('id')
    .trim()
    .not().isEmpty()
    .withMessage('Id cannot be empty')
]

exports.getUserValidator = [
    body('id')
    .trim()
    .not().isEmpty()
    .withMessage('User Id cannot be empty')
]
exports.validate = (req, res, next)=>{
    const error = validationResult(req)
    if(!error.isEmpty()){
        return res.status(400).send(error.errors);
    }
    return next();
}

