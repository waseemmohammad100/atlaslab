const express = require('express');
const routes = express.Router();

//Validators
const { addUserValidator, validate, getUserValidator } = require('../validators/userValidator');

//Controllers
const userController = require('../controllers/userController');

routes.post(/addUser\//,
    addUserValidator,
    validate,
    userController.addUser
);

routes.delete(/deleteUser\/(\d+)\//,
    userController.deleteUser
);

routes.post(/getById\//,
    getUserValidator,
    validate,
    userController.getById
);

routes.put(/editById\//,
    addUserValidator,
    userController.editUser
)

routes.get(/getAll\//,
    userController.getAll
)

exports.router = routes;