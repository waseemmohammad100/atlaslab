const User = require("../models/userModel");

exports.addUser = async (req, res, next) => {
  const data = req.body;
  try {
    let count = await User.countDocuments({});
    data.id = count + 1;
    if (data.contactNo == "") delete data.contactNo;
    const user = new User(data);

    await user.save();
    res.status(201).send({ message: "User successfully added to system" });
  } catch (err) {
    res.send(err);
    console.log(err);
  }
};

exports.deleteUser = async (req, res, next) => {
  const id = req.params[0];
  if (id) {
    console.log(id);
    try {
      const user = await User.findOne({ id: id });
      if (user) {
        const user = await User.findOneAndDelete({ id: id });
        return res
          .status(201)
          .send({ message: `user ${id} is successfully deleted` });
      }
      return res.status(404).send({ message: "user not found!" });
    } catch (err) {
      console.log(err);
      return res.send(err);
    }
  }
  return res.status(400).send({ message: "No id found!" });
};

exports.editUser = async (req, res, next) => {
  console.log(req.body);
  const id = req.body.id;
  const modifications = req.body;
  delete modifications.id;
  try {
    const user = await User.findOne({ id: id });
    if (user) {
      await User.findOneAndUpdate({ id: id }, modifications, {
        runValidators: true
      });
      return res
        .status(200)
        .send({ message: `user ${id} successfully updated` });
    }
    return res.status(404).send({ message: "user not found!" });
  } catch (err) {
    console.log(err);
    return res.status(500).send(err);
  }
};

exports.getAll = async (req, res, next) => {
  console.log("came to get all");
  try {
    const users = await User.find({}).select("-_id -__v");
    return res.status(200).send(users);
  } catch (err) {
    console.log(err);
    return res.status(500).send(err);
  }
};
exports.getById = async (req, res, next) => {
  const ID = req.body.id;
  try {
    const user = await User.findOne({ id: ID }).select("-_id -__v");
    if (user) {
      return res.status(200).send(user);
    }
    return res.status(404).send({ msg: "user not found!" });
  } catch (err) {
    console.log(err);
  }
};
