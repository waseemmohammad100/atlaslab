import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-view-all",
  templateUrl: "./view-all.component.html",
  styleUrls: ["./view-all.component.css"]
})
export class ViewAllComponent implements OnInit {
  users = [];
  addHidden = true;
  modifyHidden = true;
  addUserForm = this.fb1.group({
    name: ["", [Validators.required]],
    email: ["", [Validators.required]],
    contactNo: [""]
  });

  modifyUserForm = this.fb2.group({
    id: [{ disabled: true }],
    name: ["", [Validators.required]],
    email: ["", [Validators.required]],
    contactNo: [""]
  });

  constructor(
    private http: HttpClient,
    private fb1: FormBuilder,
    private fb2: FormBuilder
  ) {}

  async ngOnInit() {
    try {
      this.users = await this.http
        .get<[any]>("http://localhost:3000/api/getAll/")
        .toPromise();
      console.log(this.users);
    } catch (err) {
      console.log(err);
    }
  }

  addUser() {
    this.addHidden = false;
    this.modifyHidden = true;
  }

  modifyUser(user) {
    this.modifyHidden = false;
    this.addHidden = true;
    this.modifyUserForm.patchValue(user);
  }

  async submitModifications() {
    try {
      const success = await this.http
        .put("http://localhost:3000/api/editById/", this.modifyUserForm.value)
        .toPromise();
      this.ngOnInit();
      this.modifyHidden = true;
    } catch (err) {
      console.log(err);
    }
  }

  async submitAddUser() {
    try {
      const success = await this.http
        .post("http://localhost:3000/api/addUser/", this.addUserForm.value)
        .toPromise();
      this.ngOnInit();
      this.addHidden = true;
    } catch (err) {
      console.log(err);
    }
  }

  modifyCancel() {
    this.modifyHidden = true;
  }

  async deleteUser(userId: Number){
    try{
      await this.http.delete(`http://localhost:3000/api/deleteUser/${userId}/`)
      .subscribe(next=>{
        this.ngOnInit();
      },
      error=>{throw error})
    }catch(err){
      console.log(err);
    }
  }
}
